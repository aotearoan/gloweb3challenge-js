import Web3 from 'web3';
import DERC20_ABI from '../abis/erc20.json';

const MORALIS_API = 'https://speedy-nodes-nyc.moralis.io/41181ecac1cb9eb4655f31a3/polygon/mumbai';
const WALLET_ADDRESS = '';
const WALLET_PRIVATE_KEY = '';
const DERC20_CONTRACT_ADDRESS = '0xfe4F5145f6e09952a5ba9e956ED0C25e3Fa4c7F1';

const web3 = new Web3(MORALIS_API);
const tokenContract = new web3.eth.Contract(DERC20_ABI, DERC20_CONTRACT_ADDRESS);
const verifiedHumanTxs = [];

async function init() {
  web3.eth.defaultAccount = WALLET_ADDRESS;

  const maticBalance = await web3.eth.getBalance(WALLET_ADDRESS);
  const tokenBalance = await tokenContract.methods.balanceOf(WALLET_ADDRESS).call();
  console.log('Showing balance for testnet wallet');
  console.log(`MATIC balance: ${Web3.utils.fromWei(maticBalance, 'ether')}`);
  console.log(`DERC20 balance: ${Web3.utils.fromWei(tokenBalance, 'ether')}`);
  await approveDistribution();
  await submitHumanInfo();
  const humans = await readHumanInfo();
  console.log(`Humans [${humans.length}]`);
  console.log('------');
  await Promise.all(
      humans.map((human) => {
          return sendGLODistribution(human.wallet);
      })
  );
  console.log('Distribution complete');
}

async function submitHumanInfo() {
    return Promise.all(
        Object.entries(humanInfo()).map(([key, value]) => {
            const human = value;
            console.log('submitting human', human);
            return signAndSend(
                WALLET_ADDRESS,
                WALLET_ADDRESS,
                web3.utils.toHex(JSON.stringify(human)),
                (receipt) => {
                    console.log(`Successfully submitted verified human tx: ${receipt.transactionHash}`);
                    // Caching tx hashes for the purpose of this exercise (so I don't have to iterate over every block seeking the verified humans)
                    verifiedHumanTxs.push(receipt.transactionHash);
                },
            );
        }),
    );
}

async function readHumanInfo() {
  return Promise.all(
      verifiedHumanTxs.map(
          (hash) => web3.eth.getTransaction(hash)
              .then((tx) => JSON.parse(web3.utils.hexToUtf8(tx.input)))
      )
  );
}

async function approveDistribution() {
  const abi = tokenContract.methods.approve(WALLET_ADDRESS, '100000000000000000000').encodeABI();

  return signAndSend(
      WALLET_ADDRESS,
      DERC20_CONTRACT_ADDRESS,
      abi,
      (receipt) => {
        console.log(`Submitted approved spending tx: ${receipt.transactionHash}`);
      }
  );
}

async function sendGLODistribution(to) {
  const abi = tokenContract.methods.transferFrom(WALLET_ADDRESS, to, '1000000000000000000').encodeABI();

  return signAndSend(
      WALLET_ADDRESS,
      DERC20_CONTRACT_ADDRESS,
      abi,
      (receipt) => {
        console.log(`Submitted distribute GLO to ${to}, tx: ${receipt.transactionHash}`);
      }
  );
}

async function signAndSend(from, to, data, callback) {
  const tx = await web3.eth.accounts.signTransaction(
      {
        from,
        to,
        gas: '150000',
        data,
      },
      WALLET_PRIVATE_KEY,
  );

  // TODO: listen to event callbacks once transactions are verified
  return web3.eth.sendSignedTransaction(tx.rawTransaction).then((receipt) => callback(receipt));
}

function humanInfo() {
  return {
    user123: {
      name: 'John Doe Of Denham',
      dob: '10-10-1980',
      nationality: 'NL',
      email: 'john.doe@denham.com',
      tel: '123',
      cob: 'abc',
      pob: 'def',
      country: 'ghi',
      city: 'jkl',
      street: 'mno',
      nin: 'pqr',
      idn: 'stu',
      kyc_date: new Date().toISOString().split('T')[0],
      wallet: '0xD6B2fed043153cC9A4698773b8721237626ecF29',
    }
  };
}


init();


